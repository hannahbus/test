Algorithm Reference
=======================

Modified Causal Forest
----------------------

.. toctree::
    :maxdepth: 1
    :numbered:

    algorithm_reference/training
    algorithm_reference/bgates_cbgates
    algorithm_reference/inference
    algorithm_reference/local_centering

Optimal Policy
--------------

.. toctree::
    :maxdepth: 1
    :numbered:

    algorithm_reference/optimal-policy_algorithm